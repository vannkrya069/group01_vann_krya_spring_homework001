package com.example.group01_vann_krya_spring_homework001.rest_api;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Objects;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    ArrayList<Customer> customers = new ArrayList<>(){{
        add(new Customer("Leo Messi", "Male", 36, "Paris"));
        add(new Customer("Mike Kwamdee", "Male", 32, "Bangkok"));
        add(new Customer("Monica Julie", "Female", 25, "London"));
    }};


    @PostMapping
    public ResponseEntity<?> insertCustomer(@RequestBody CustomerInsert customerinsert) {
        customers.add(new Customer(customerinsert));
        return  ResponseEntity.ok(new CustomerResponse<Customer>(
                "This record was successfully created",
                customers.get(customers.size()-1),
                "OK",
                LocalDateTime.now()
        ));
    }


    @GetMapping
    public ResponseEntity<?> getAllCustomer() {
        if(customers.isEmpty()){
            return new ResponseEntity<>("No customers in list...",HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(new CustomerResponseGetAll<ArrayList<Customer>>(
                "You get all customers successfully",
                customers,
                "OK",
                LocalDateTime.now()
        ));
    }


    @GetMapping("{customerId}")
    public ResponseEntity<?> getCustomerById(@PathVariable int customerId) {
        for (Customer customer : customers
             ) {
            if(customer.getId() == customerId) {
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "This record has found successfully",
                        customer,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return new ResponseEntity<>("Id not found...",HttpStatus.NOT_FOUND);
    }


    @GetMapping("/search")
    public ResponseEntity<?> getCustomerByName(@RequestParam String customerName) {
        for (Customer customer : customers
        ) {
            if(Objects.equals(customer.getName(), customerName)) {
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "This record has found successfully",
                        customer,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return new ResponseEntity<>("Name not found...",HttpStatus.NOT_FOUND);
    }


    @PutMapping("{customerId}")
    public ResponseEntity<?> updateCustomerById(@RequestParam int customerId, @RequestBody CustomerInsert customerinsert) {
        for (Customer customer : customers
        ) {
            if(customer.getId() == customerId) {
                customer.setName(customerinsert.getName());
                customer.setGender(customerinsert.getGender());
                customer.setAge(customerinsert.getAge());
                customer.setAddress(customerinsert.getAddress());
                customers.replaceAll(customerUpdate -> customerUpdate);
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "You're update successfully",
                        customer,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return new ResponseEntity<>("Id not found...",HttpStatus.NOT_FOUND);
    }


    @DeleteMapping("{customerId}")
    public ResponseEntity<?> deleteCustomerById(@RequestParam int customerId) {
        for (Customer customer : customers
        ) {
            if(customer.getId() == customerId) {
                customers.remove(customer);
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "Congratulation your delete is successfully",
                        customer,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return new ResponseEntity<>("The save with update not found id...",HttpStatus.NOT_FOUND);
    }
}
