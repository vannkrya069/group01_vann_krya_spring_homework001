package com.example.group01_vann_krya_spring_homework001.rest_api;

import java.time.LocalDateTime;

public class CustomerResponseGetAll<T> {
    private String message;
    private T customers;
    private String status;
    private LocalDateTime time;

    public CustomerResponseGetAll(String message, T customers, String status, LocalDateTime time) {
        this.message = message;
        this.customers = customers;
        this.status = status;
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomers() {
        return customers;
    }

    public void setCustomers(T customers) {
        this.customers = customers;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }
}
