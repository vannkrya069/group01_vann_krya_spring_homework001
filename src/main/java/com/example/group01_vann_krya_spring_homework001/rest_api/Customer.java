package com.example.group01_vann_krya_spring_homework001.rest_api;

public class Customer {
    private int id;
    private String name;
    private String gender;
    private int age;
    private String address;
    static int autoId = 0;

    public Customer(String name, String gender, int age, String address) {
        this.id = ++autoId;
        this.name = name;
        this.gender = gender;
        this.age = age;
        this.address = address;
    }

    public Customer(CustomerInsert customerinsert) {
        this.id = ++autoId;
        this.name = customerinsert.getName();
        this.gender = customerinsert.getGender();
        this.age = customerinsert.getAge();
        this.address = customerinsert.getAddress();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
